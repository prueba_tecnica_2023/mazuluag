secuencia = []

while True:
    commat_int = int(input("Ingrese un número (para terminar ingrese -1): "))
    if commat_int != -1:
        secuencia.append(commat_int)
    else:
        break

prom_sec = sum(secuencia) / len(secuencia)
prom_valores_mayores = 0
cont = 0
for i in secuencia:
    if i > prom_sec:
        prom_valores_mayores += i
        cont += 1

prom_valores_mayores = prom_valores_mayores / cont
print("Promedio de los valores mayores al promedio de la secuencia es: " + str(prom_valores_mayores))