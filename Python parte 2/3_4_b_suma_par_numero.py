def sum_par_num (array, x):
    closest_sum = 0
    closest_pair = ()

    for i in range(len(array)-1):
        for j in range(1,len(array)):
            current_sume = array[i] + array[j]
            if abs(current_sume - x) < abs(closest_sum - x):
                closest_sum = current_sume
                closest_pair = (array[i], array[j])
    print("Los número mas cercano al valor {} son ({}, {})".format(x,closest_pair[0],closest_pair[1])) 


L = [10, 22, 28, 29, 30, 40]
x = 54
sum_par_num(L,x)