def get_ring_elements(matrix, layer, rows, columns):
    """Función que obtiene el anillo por las capaz que tenga la matriz

    Args:
        matrix (array): lista MxN
        layer (int): capa
        rows (int): número de filas
        columns (int): número de columnas

    Returns:
        elements (list): lista con los datos de la capaz
    """

    elements = []
    for i in range(layer, rows - layer):
        elements.append(matrix[i][layer])
    for j in range(layer + 1, columns - layer):
        elements.append(matrix[rows - 1 - layer][j])
    for i in range(rows - 2 - layer, layer - 1, -1):
        elements.append(matrix[i][columns - 1 - layer])
    for j in range(columns - 2 - layer, layer, -1):
        elements.append(matrix[layer][j])
    return elements

def put_ring_elements(matrix, layer, rows, columns, rotated_elements,index):
    """Función que construye la matriz según los la variable rotated_elements

    Args:
        matrix (array): lista MxN
        layer (int): capa
        rows (int): número de filas
        columns (int): número de columnas
        rotated_elements (list): lista con los datos de la capaz rotados
        index (int): indece de la matriz

    Returns:
        elements (list): lista con los datos de la capaz
    """
    for i in range(layer, rows - layer):
        matrix[i][layer] = rotated_elements[index]
        index += 1
    for j in range(layer + 1, columns - layer):
        matrix[rows - 1 - layer][j] = rotated_elements[index]
        index += 1
    for i in range(rows - 2 - layer, layer - 1, -1):
        matrix[i][columns - 1 - layer] = rotated_elements[index]
        index += 1
    for j in range(columns - 2 - layer, layer, -1):
        matrix[layer][j] = rotated_elements[index]
        index += 1
    return matrix



def rotate_matrix(matrix, p):
    """Función que para rotar matrix segun el valor p, donde La rotación debe ser en sentido 
        antihorario si p < 0 y en sentido horario cuando p > 0.
    Args:
        matrix (array): lista MxN
        p (int): Sentido y cantidad de giro
    Returns:
        None
    """
    rows = len(matrix)
    columns = len(matrix[0])

    if rows >= 2 and columns <= 100:
        if abs(p) >= 1 and abs(p) <= 10^7:
            if min(rows,columns) % 2 == 0:
                # Se realiza un recorrido por capaz según el tamaño de la matriz
                for layer in range(min(rows,columns) // 2):
                    ring_elements = get_ring_elements(matrix, layer, rows, columns)
                    # Determina el tipo de sentido de rotación
                    if p < 0:
                        p = abs(p)
                        rotated_elements = ring_elements[-p:] + ring_elements[:-p]
                    else:
                        rotated_elements = ring_elements[p:] + ring_elements[:p]
                    index = 0
                    #Construir la matriz de nuevo
                    matrix = put_ring_elements(matrix,layer,rows,columns,rotated_elements,index)
            else:
                print("No cumple con la condición de min(m, n)%2 = 0")
        else:
            print("No cumple con la condición de 1 <= p <= 10^7")
    else:
        print("No cumple con la condición de 2 <= m, n <= 100")

    for i in range(len(matrix)):  
        print(matrix[i])


matriz_original = [
    [1, 2, 3,4],
    [5, 6, 7,8],
    [9, 10, 11,12],
    [13,14,15,16]
]

p = -2 
rotate_matrix(matriz_original, p)
