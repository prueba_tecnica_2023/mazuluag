SELECT
T1.radicado, T1.num_documento, T1.cod_segm_tasa, T1.cod_subsegm_tasa, T1.cal_interna_tasa,
T1.id_producto, T1.tipo_id_producto, T1.valor_inicial, T1.fecha_desembolso, T1.plazo,
T1.cod_periodicidad, T1.periodicidad, T1.saldo_deuda, T1.modalidad, T1.tipo_plazo,
IF(T1.id_producto LIKE "%operacion_especifica%", T2.tasa_operacion_especifica,
IF(T1.id_producto LIKE "%Cartera%", T2.tasa_cartera,
IF(T1.id_producto LIKE "%leasing%", T2.tasa_leasing,
IF(T1.id_producto LIKE "%tarjeta%", T2.tasa_tarjeta,
IF(T1.id_producto LIKE "%Hipotecario%", T2.tasa_hipotecario,
IF(T1.id_producto LIKE "%Sufi%", T2.tasa_sufi,
IF(T1.id_producto LIKE "%factoring%", T2.tasa_factoring, NULL))))))) AS Tasa
FROM prueba_tecnica.obligaciones_clientes T1
INNER JOIN prueba_tecnica.tasas_productos T2 ON T1.cod_segm_tasa = T2.cod_segmento AND T1.cod_subsegm_tasa = T2.cod_subsegmento AND T1.cal_interna_tasa = T2.calificacion_riesgos;